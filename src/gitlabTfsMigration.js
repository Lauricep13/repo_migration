const Logger = require("js-logger");
const { getLogLevel } = require("./lib/config");
const gitTfs = require("./lib/gitTfs");
const gitlab = require("./lib/gitlab");
const readline = require("readline").createInterface({
  input: process.stdin,
  output: process.stdout,
});
const safeJsonStringify = require("safe-json-stringify");
const { getExcelIssueData } = require('./lib/excel');

require("dotenv").config();

exports.migrateGitlabProjects = async ({
  logLevel,
  tfsCollectionUrl,
  projectsToMigrate,
  mirationStartIndex,
  gitlabAccessToken,
  projectNameSuffix,
  projectVisibility,
  projectNamespaceId,
  gitlabHost,
}) => {
  try {
    Logger.setDefaults();
    Logger.setLevel(getLogLevel(logLevel));

    // List all the repositories
    const projects = await gitTfs.listRemoteBranches({
      repositoryUrl: tfsCollectionUrl,
    });

    for (let i = mirationStartIndex; i < projects.length; i++) {
      if (projectsToMigrate != -1 && i >= projectsToMigrate) break;

      const project = projects[i];
      Logger.info(`Migrating project ${project.projectBranch} to gitlab`);

      // Create the gitlab project
      await gitlab.createGitlabProjectIfNecessary({
        project,
        gitlabAccessToken,
        projectNameSuffix,
        projectVisibility,
        projectNamespaceId,
        gitlabHost,
      });
    }

    readline.question("done?", () => {
      readline.close();
    });
  } catch (error) {
    console.log("Error: ", safeJsonStringify(error, null, 2));
    readline.question("done?", () => {
      readline.close();
    });
  }
};

exports.migrateRepositories = async ({
  logLevel,
  clonedRepositoryDirectory,
  tfsCollectionUrl,
  projectsToMigrate,
  mirationStartIndex,
  gitlabAccessToken,
  projectNameSuffix,
  gitlabHost,
}) => {
  try {
    Logger.setDefaults();
    Logger.setLevel(getLogLevel(logLevel));

    // List all the repositories
    const projects = await gitTfs.listRemoteBranches({
      repositoryUrl: tfsCollectionUrl,
    });

    for (let i = mirationStartIndex; i < projects.length; i++) {
      if (projectsToMigrate != -1 && i >= projectsToMigrate) break;

      const project = projects[i];
      Logger.debug(`Migrating project ${project.project} to gitlab`);

      // Create the gitlab project
      const foundProject = await gitlab.findProject({
        gitlabAccessToken,
        project,
        projectNameSuffix,
        gitlabHost,
      });

      if (!foundProject) {
        throw new Error(
          `Project ${project.project} could not be found in gitlab`
        );
      }

      const outputDir = `${clonedRepositoryDirectory}/${hashCode(
        `${project.rawProjectName}/${project.rawBranchName}`
      )}`;
      // Clone the TFS repository
      await gitTfs.clone({
        repositoryUrl: tfsCollectionUrl,
        branch: project.rawBranchName,
        rawProjectName: project.rawProjectName,
        repositoryPath: outputDir,
      });

      // Add remote origin in git
      await gitlab.addRemoteOriginUrlIfNecessary({
        remoteUrl: foundProject.http_url_to_repo,
        clonedRepositoryDirectory: outputDir,
      });

      // Push the code to Gitlab
      await gitTfs.push({ clonedRepositoryDirectory: outputDir });
    }

    Logger.info("Done Migrating Projects");

    readline.question("done?", () => {
      readline.close();
    });
  } catch (err) {
    Logger.error("Error: ", safeJsonStringify(err, null, 2));
    readline.question("done?", () => {
      readline.close();
    });
  }
};

const hashCode = (s) =>
  s.split("").reduce((a, b) => ((a << 5) - a + b.charCodeAt(0)) | 0, 0);

exports.loadUserStories = async ({ filePath, gitlabAccessToken, gitlabHost, logLevel }) => {
  try {
    Logger.setDefaults();
    Logger.setLevel(getLogLevel(logLevel));

    const issues = await getExcelIssueData(filePath);

    for (let i = 0; i < issues.length; i++) {
      const issue = issues[i];

      await gitlab.createIssue({
        gitlabHost,
        gitlabAccessToken,
        ...issue
      });

      Logger.info(`Issues created ${i + 1}/${issues.length}`)
    }

    Logger.info("All issues created");
  } catch (error) {
    Logger.error("Error creating issue", error);
  }
};

