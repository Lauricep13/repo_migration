const yargs = require("yargs/yargs");
const { hideBin } = require("yargs/helpers");
const { loadUserStories } = require("./gitlabTfsMigration");
const gitlabTfsMigration = require('./gitlabTfsMigration');

yargs(hideBin(process.argv))
  .command('migrate', 'Migrate TFS to Gitlab', (yargs) => {
    return yargs
      .option('tfsCollectionUrl', {
        describe: 'The TFS Url including the collection name'
      })
      .option('clonedRepositoryDirectory', {
        describe: 'The temporary folder to hold the cloned repositories',
        default: './tmp'
      })
      .option('logLevel', {
        describe: 'Sets the log level',
        default: 'INFO'
      })
      .choices('logLevel', ['INFO', 'DEBUG'])
      .option('projectsToMigrate', {
        describe: 'The number of projects to migrate',
        default: 1
      })
      .option('mirationStartIndex', {
        describe: 'The start index for the projects to migrate.',
        default: 0
      })
      .option('gitlabAccessToken', {
        describe: "",
      })
      .option('projectNameSuffix', {
        describe: "The suffix to add to the Gitlab project names",
        default: "DEV"
      })
      .option('projectVisibility', {
        describe: "The visibility to set for each Gitlab project migrated",
        default: "private"
      })
      .option('projectNamespaceId', {
        describe: "The Gitlab group id you want all projects to be migrated under"
      })
      .option('gitlabHost', {
        describe: "The gitlab host",
        default: "https://gitlab.com"
      })
      .demandOption(['tfsCollectionUrl'], 'Please provide both run and path arguments to work with this tool')
      .example([
        ["$0 migrate --tfsCollectionUrl 'http://desktop-viao02c:8080/tfs/defaultcollection' --projectNamespaceId 13481198"]
      ])
  }, gitlabTfsMigration.migrateRepositories)
  .command('projects', 'Migrate TFS to Gitlab', (yargs) => {
    return yargs
      .option('tfsCollectionUrl', {
        describe: 'The TFS Url including the collection name eg. http://desktop-viao02c:8080/tfs/defaultcollection',
        default: "http://desktop-viao02c:8080/tfs/defaultcollection",
      })
      .option('clonedRepositoryDirectory', {
        describe: 'The temporary folder to hold the cloned repositories',
      })
      .option('logLevel', {
        describe: 'Sets the log level',
        default: 'INFO'
      })
      .choices('logLevel', ['INFO', 'DEBUG'])
      .option('projectsToMigrate', {
        describe: 'The number of projects to migrate',
        default: 1
      })
      .option('mirationStartIndex', {
        describe: 'The start index for the projects to migrate.',
        default: 0
      })
      .option('gitlabAccessToken', {
        describe: "",
        default: ""
      })
      .option('gitlabHost', {
        describe: "The gitlab host",
        default: "https://gitlab.com"
      })
      .option('projectNameSuffix', {
        describe: "The suffix to add to the Gitlab project names",
        default: "DEV"
      })
  }, gitlabTfsMigration.migrateGitlabProjects)
  .command(
    "user-stories",
    "Migrate TFS to Gitlab",
    (yargs) => {
      return yargs
        .option("filePath", {
          describe: "The TFS Url including the collection name",
          default: "",
        })
        .option("logLevel", {
          describe: "Sets the log level",
          default: "INFO",
        })
        .choices("logLevel", ["INFO", "DEBUG"])
        .option("gitlabAccessToken", {
          describe: "",
          default: "",
        })
        .option("gitlabHost", {
          describe: "The gitlab host",
          default: "https://gitlab.com",
        });
    },
    loadUserStories
  )
  .parse();
