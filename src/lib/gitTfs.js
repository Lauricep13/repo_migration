const util = require("util");
const exec = util.promisify(require("child_process").exec);
const _ = require("lodash");
const fs = require("fs");
const Logger = require("js-logger");

/**
 * Function to execute exe
 * @param {String} command The name of the executable file to run.
 * @returns {Promise<Object>} something
 */
execute = async (command, { cwd } = {}) => {
  Logger.debug(`Executing command: ${command}`);
  const { stdout, stderr } = await exec(command, { cwd });
  Logger.debug("stdout: ", stdout);
  Logger.debug("stderr: ", stderr);

  return stdout;
};

/**
 * @param {Object} arg
 * @param {String} arg.repositoryUrl
 * @param {String} arg.branch
 * @param {String} arg.repositoryPath
 * @returns {Promise<String>}
 */
exports.clone = async ({
  repositoryUrl,
  rawProjectName,
  branch,
  repositoryPath = "./tmp",
}) => {
  if (!fs.existsSync(repositoryPath)) {
    fs.mkdirSync(repositoryPath, { recursive: true });
  }

  Logger.debug(`Cloning TFS repository ${repositoryUrl}`);
  return await execute(
    `git tfs clone "${repositoryUrl}" "${rawProjectName}/${branch}" "${repositoryPath}"`
  );
};

/**
 * @param {Object} arg
 * @param {String} arg.clonedRepositoryDirectory
 * @returns {Promise<String>}
 */
exports.listRemotePushUrls = async ({ clonedRepositoryDirectory }) => {
  try {
    return await execute(`git remote get-url --push origin`, {
      cwd: clonedRepositoryDirectory,
    });
  } catch (error) {
    Logger.debug(error);
    return "";
  }
};

/**
 *
 * @param {Object} arg
 * @param {String} arg.repositoryUrl
 * @returns {Promise<Array<{ project: String, branch: String, projectBranch: String }>>}
 */
exports.listRemoteBranches = async ({ repositoryUrl }) => {
  Logger.debug("Listing remote branches");

  const resultString = await execute(
    `git tfs list-remote-branches "${repositoryUrl}"`
  );

  const projects = findAllProjectsAndBranches(resultString);

  Logger.debug("Remote branches found:", JSON.stringify(projects, null, 2));

  return projects;
};

/**
 * @param {Object} arg
 * @param {String} arg.remoteUrl
 * @param {String} arg.clonedRepositoryDirectory
 * @returns {Promise<String>}
 */
exports.addRemoteOrigin = async ({ remoteUrl, clonedRepositoryDirectory }) => {
  Logger.debug("Adding remote origin");
  return await execute(`git remote add origin "${remoteUrl}"`, {
    cwd: clonedRepositoryDirectory,
  });
};

/**
 * @param {Object} arg
 * @param {String} arg.remoteUrl
 * @param {String} arg.clonedRepositoryDirectory
 * @returns {Promise<String>}
 */
exports.checkoutBranch = async ({ branchName, clonedRepositoryDirectory }) => {
  Logger.debug("Checking out branch " + branchName);
  return await execute(`git checkout -b ${branchName}`, {
    cwd: clonedRepositoryDirectory,
  });
};

/**
 * @param {Object} arg
 * @param {String} arg.clonedRepositoryDirectory
 * @returns {Promise<String>}
 */
exports.push = async ({ clonedRepositoryDirectory }) => {
  Logger.info("Pushing changes to Gitlab");
  try {
    return await execute(`git push -u --all origin`, {
      cwd: clonedRepositoryDirectory,
    });
  } catch (error) {
    Logger.error(error);
  }
};

/**
 * @param {String} string
 * @returns {Array<{ project: String, branch: String, projectBranch: String, rawProjectName: String }>}
 */
findAllProjectsAndBranches = (string) => {
  const regex =
    /(?<projectBranch>\$\/(?<projectName>.+)(\/.*)?\/(?<branchName>[a-zA-Z0-9-]+))/g;
  const results = [];
  let match;

  while ((match = regex.exec(string))) {
    results.push({
      rawProjectName: `$/${match.groups.projectName}`,
      rawBranchName: match.groups.branchName,
      project: match.groups.projectName.replace(/\s/g, "_").replace(/\//g, "-"),
      branch: match.groups.branchName.replace(/\s/g, "_").replace(/\//g, "-"),
      projectBranch: match.groups.projectBranch
        .replace(/\s/g, "_")
        .replace(/\//g, "-"),
    });
  }

  return results;
};
