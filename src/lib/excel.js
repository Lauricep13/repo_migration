const Logger = require("js-logger");
const readXlsxFile = require("read-excel-file/node");

exports.getExcelIssueData = async (filePath) => {
    const rows = await readXlsxFile(filePath);
    Logger.debug("row data from excel: ", JSON.stringify(rows, null, 2));

    const issues = [];

    if (rows.length == 0) {
        throw new Error("Missing header row in excel file");
    }

    for (let i = 1; i < rows.length; i++) {
        const row = rows[i];

        const issue = {};
        for (let j = 0; j < row.length; j++) {
            const data = row[j];
            const header = rows[0][j];

            if (!header)
                continue;

            issue[header] = data;
        }

        issues.push(issue);
    }

    Logger.debug("Issues after formatting: ", JSON.stringify(issues, null, 2));
    return issues;
};
