const Logger = require('js-logger');

exports.getLogLevel = (logLevel) => {
    switch (logLevel.toUpperCase()) {
        case "OFF":
            return Logger.OFF
        case "INFO":
            return Logger.INFO
        case "DEBUG":
            return Logger.DEBUG
        case "ERROR":
            return Logger.ERROR
        case "WARN":
            return Logger.WARN
        default:
            throw new Error(`Unknown level ${level}`);
    }
}