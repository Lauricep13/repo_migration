const { Gitlab } = require("@gitbeaker/node");
const Logger = require("js-logger");
const gitTfs = require("./gitTfs");

/**
 * @param {String} http_url_to_repo
 * @returns {Promise}
 */
exports.addRemoteOriginUrlIfNecessary = async ({
  remoteUrl,
  clonedRepositoryDirectory,
}) => {
  const remotePushUrls = await gitTfs.listRemotePushUrls({
    clonedRepositoryDirectory,
  });

  Logger.debug("List Remote Prush url response", remotePushUrls);
  if (remotePushUrls.includes(remoteUrl))
    Logger.info("Remote push url origin already exists");
  else await gitTfs.addRemoteOrigin({ remoteUrl, clonedRepositoryDirectory });
};

/**
 * @param {Object} arg
 * @param {Object} arg.project
 * @param {String} arg.project.project
 * @param {String} arg.project.branch
 * @param {String} arg.project.projectBranch
 * @param {String} arg.gitlabAccessToken
 * @returns {Promise<String>}
 */
exports.createGitlabProjectIfNecessary = async ({
  project,
  gitlabAccessToken,
  projectNameSuffix,
  projectVisibility,
  projectNamespaceId,
  gitlabHost,
}) => {
  const projectName = getProjectName(project, projectNameSuffix);
  const foundProject = await this.findProject({
    gitlabAccessToken,
    project,
    projectNameSuffix,
    gitlabHost,
  });
  let http_url_to_repo;

  if (!foundProject) {
    Logger.info(`Creating Gitlab project ${projectName}`);
    const gitlab = new Gitlab({ token: gitlabAccessToken, host: gitlabHost });
    const response = await gitlab.Projects.create({
      name: projectName,
      path: projectName,
      visibility: projectVisibility,
      namespaceId: projectNamespaceId,
    });

    http_url_to_repo = response.http_url_to_repo;
  } else {
    Logger.debug(`Gitlab project ${projectName} already exists`);
    http_url_to_repo = foundProject.http_url_to_repo;
  }

  return http_url_to_repo;
};

exports.findProject = async ({
  gitlabAccessToken,
  project,
  projectNameSuffix,
  gitlabHost,
}) => {
  const gitlab = new Gitlab({ token: gitlabAccessToken, host: gitlabHost });
  const projectName = getProjectName(project, projectNameSuffix);
  const foundProjects = await gitlab.Projects.search(projectName);

  if (foundProjects.length == 0) {
    Logger.debug(`No projects found`);
    return null;
  } else if (foundProjects.length == 1) {
    Logger.debug(`Found 1 project`);
    return foundProjects[0];
  } else {
    throw new Error(`Found to many projects with the name ${projectName}`);
  }
};

function getProjectName(project, projectNameSuffix) {
  const projectName = `${project.project}-${projectNameSuffix}`.replace(
    /\//g,
    "-"
  );
  Logger.debug(`Checking if project ${projectName} exists`);
  return projectName;
}

exports.createIssue = async ({
  projectId,
  description,
  dueDate,
  labels,
  issueType,
  weight,
  milestoneId,
  createdAt,
  assigneeId,
  gitlabAccessToken,
  gitlabHost,
  title
}) => {
  Logger.debug(`Creating Issue with title '${title}'`);
  const gitlab = new Gitlab({ token: gitlabAccessToken, host: gitlabHost });
  const users = await this.findUsers({ gitlabAccessToken, gitlabHost, userName: assigneeId });
  const userId = users?.length > 0 ? users[0].id : null;

  return gitlab.Issues.create(projectId, {
    description,
    due_date: dueDate,
    labels,
    issue_type: issueType,
    weight,
    milestone_id: milestoneId,
    created_at: createdAt,
    assignee_id: userId,
    title
  });
};

exports.findProject = async ({ gitlabAccessToken, projectId, gitlabHost }) => {
  const gitlab = new Gitlab({ token: gitlabAccessToken, host: gitlabHost });

  if (foundProjects.length == 0) {
    Logger.debug(`No projects found`);
    return null;
  } else if (foundProjects.length == 1) {
    Logger.debug(`Found 1 project`);
    return foundProjects[0];
  } else {
    throw new Error(`Found to many projects with the name ${projectName}`);
  }
};

exports.findUsers = async ({ gitlabAccessToken, gitlabHost, userName }) => {
  try {
    const gitlab = new Gitlab({ token: gitlabAccessToken, host: gitlabHost });
    const users = await gitlab.Users.username(userName);
    Logger.debug(`User found: ${JSON.stringify(users)}`);

    return users;
  } catch (error) {
    Logger.debug(`Error finding user: ${userName}`, error);
    return null;
  }
};
